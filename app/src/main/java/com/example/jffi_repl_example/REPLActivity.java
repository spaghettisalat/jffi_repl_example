package com.example.jffi_repl_example;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import jffi.Lisp;
import jffi.LispCondition;
import jffi.LispEnvironmentError;

public class REPLActivity extends AppCompatActivity {
	// Load the shared libraries. The jffi library is already
	// automatically loaded by Lisp.java.
	static {
		System.loadLibrary("ecl");
		System.loadLibrary("lisp_init");
	}

	private TextView output;
	private EditText input;
	private ScrollView scrollView;
	// We use custom streams to communicate with the Lisp REPL. Via a
	// wrapper for the Lisp stream interface, writer becomes the Lisp
	// standard output and reader the standard input.
	private REPLOutputWriter writer;
	private REPLInputReader reader;

	// In which subdirectory are the Lisp assets ...
	private static String ASSETS_FROM_DIR = "lisp";
	// ... and where to put them on the phone
	private static String ASSETS_TO_DIR = "resources";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_repl);
		output = (TextView) findViewById(R.id.repl_output);
		// Add a Listener to scroll to the bottom if output is appended
		output.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {
					// We have to post this with a delay, because else the
					// output TextView won't have changed its dimensions
					// yet when the scroll action is executed and the
					// ScrollView will not scroll all the way to the
					// bottom.
					scrollView.postDelayed(new Runnable() {
							public void run() {
								scrollView.smoothScrollTo(0, output.getHeight());
							}
						}, 50);
				}
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				public void onTextChanged(CharSequence s, int start, int before, int count) {} 
			});
		input = (EditText) findViewById(R.id.repl_input);
		scrollView = (ScrollView) findViewById(R.id.repl_scroll_view);
		input.requestFocus();
		reader = new REPLInputReader();
		writer = new REPLOutputWriter();
		try {
			// Set the directory for optional .fas files. It is probably
			// better to unpack the assets in a separate thread as to not
			// block the main thread.
			String Ecldir = unpackAssets(ASSETS_FROM_DIR, ASSETS_TO_DIR) + "/lib/";
			Lisp.Options.setEcldir(Ecldir);
			// Start ECL. It is probably a better idea to do this in once
			// for the application in Application.onCreate(), but for
			// simplicity we do it in the activity.
			Lisp.start();
		} catch(LispEnvironmentError le) {
			showErrorWithStacktrace(getResources().getText(R.string.lisp_start_error_message) + ": " + le.getMessage(), le);
		}
		Thread replThread = new Thread() {
				public void run() {
					try {
						// Create the REPL environment and start the REPL
						Lisp.attachCurrentThread();
						Lisp.funcall("EXT", "INSTALL-BYTECODES-COMPILER");
						Lisp.funcall("LISP-REPL", "READ-EVAL-PRINT-LOOP", reader, writer);
						Lisp.detachCurrentThread();
					} catch(LispEnvironmentError l) {
						final LispEnvironmentError le = l;
						runOnUiThread(new Runnable(){
								public void run(){
									showErrorWithStacktrace(getResources().getText(R.string.lisp_environment_error_message) + ": " + le.getMessage(), le);
								}
							}); 
					} catch(LispCondition lc) {
						final String text = getResources().getText(R.string.lisp_condition_error_message) + ": " + lc.toString();
						runOnUiThread(new Runnable(){
								public void run(){
									showError(text);
								}
							}); 
					}
				}
			};
		replThread.start();
	}
	public void onStop(){
		super.onStop();
		Lisp.stop();
		reader.close();
		writer.close(); 
	}

	private static String ASSETS_UNPACKED_SETTING = "assetsUnpacked";
	// Unpack all assets out of the from directory and return the
	// destination path
	private String unpackAssets(String from, String to) {
		File destination = getDir(to, MODE_PRIVATE);
		try {
			SharedPreferences settings = getSharedPreferences("inferior", MODE_PRIVATE);
			int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode; 
			int assetsUnpacked = settings.getInt(ASSETS_UNPACKED_SETTING, 0);
			// If the unpacked assets were from a different version, we
			// overwrite them
			if (assetsUnpacked != versionCode) {
				unpackObject(new File(from), destination);
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt(ASSETS_UNPACKED_SETTING, versionCode);
				editor.commit();
			}
		} catch(Exception e) {
			showErrorWithStacktrace(getResources().getText(R.string.unpack_file_error).toString() + ": " + e.toString(), e);
		}
		return destination.getAbsolutePath();
	}
	private void unpackObject(File from, File to) throws IOException {
		String[] files = getAssets().list(from.getPath());
		if (files.length == 0) {
			// from is a file
			unpackFile(from, to);
		} else {
			// from is a directory
			to.mkdirs();
			for (String file:files) {
				unpackObject(new File(from, file), new File(to, file));
			}
		}
	}
	private void unpackFile(File from, File to) throws IOException {
		InputStream in = getAssets().open(from.getPath()); 
		OutputStream out = new FileOutputStream(to, false);
		try {
			byte[] buffer = new byte[1024];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
		} finally {
			in.close();
			out.close();
		}
	}

	public void showErrorWithStacktrace(String text, Throwable error) {
		text = text.concat("\nBacktrace:\n");
		StackTraceElement[] ste = error.getStackTrace();
		for (int i = 0; i < ste.length; i++) {
			text = text.concat(ste[i].toString() + "\n");
		}
		showError(text);
	}
	public void showError(String t) {
		final Context context = this;
		final String text = t;
		runOnUiThread(new Runnable(){
				public void run() {
					AlertDialog.Builder builder = new AlertDialog.Builder(context);
					builder.setTitle(R.string.error_dialog_title).setMessage(text);
					builder.setPositiveButton(R.string.error_dialog_button_text, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			});
	}
	
	public void evalClick(View v) {
		String command = input.getText() + "\n";
		output.append(command);
		reader.appendText(command);
		input.setText("");
	}

	private class REPLOutputWriter extends Writer {
		public REPLOutputWriter() {
			super();
		}
		public void close() {}
		public void flush() {}
		public void write(char[] cbuf, int offset, int length) {
			final String str = new String(cbuf, offset, length);
			runOnUiThread(new Runnable() {
					public void run() {
						output.append(str);
					}
				});
		}
		public void write(String s) {
			final String str = s;
			runOnUiThread(new Runnable() {
					public void run() {
						output.append(str);
					}
				});
		}
	}

	private class REPLInputReader extends Reader {
		protected StringBuilder buffer = new StringBuilder();
		// position of the mark in buffer
		protected int mark = 0;
		// position of the current character in buffer or -1 if the
		// stream has been closed
		protected int position = 0;
		protected boolean markReset = true;
		protected int markReadAheadLimit = 0; 
		public REPLInputReader() {
			super();
		}
		public synchronized void appendText(CharSequence s) {
			buffer.append(s);
			notify();
		}
		public synchronized int read(char[] cbuf, int offset, int length) {
			if (position < 0)
				return -1;
			for (int i = 0; i < length; i++) {
				while (position >= buffer.length()) {
					try{
						wait();
					} catch(InterruptedException e) {} 
				}
				cbuf[offset + i] = buffer.charAt(position++);
			}
			if (position - mark > markReadAheadLimit)
				markReset = true;
			int del;
			if (markReset) {
				del = position;
				mark = position;
			} else {
				del = Math.min(mark, position);
				markReset = true;
			}
			buffer.delete(0, del);
			position -= del;
			mark -= del;
			return length;
		}
		public boolean markSupported(){
			return true;
		}
		public synchronized void mark(int readAheadLimit){
			markReadAheadLimit = readAheadLimit;
			mark = position;
			markReset = false;
		}
		public synchronized void reset() {
			position = mark;
		}
		public synchronized void close() {
			buffer.delete(0, buffer.length());
			position = -1;
		}
		public synchronized boolean ready() {
			return position < buffer.length();
		}
	}
}
