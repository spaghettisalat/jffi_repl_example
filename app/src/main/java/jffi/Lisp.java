// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

package jffi;

public class Lisp{
	static{ 
		System.loadLibrary("jffi");
	}
	protected static boolean lisp_running = false;
	protected static native CLObject native_funcall(String packagename, String symbolname, Object[] args);
	protected static native CLObject native_object_funcall(String packagename, String symbolname, Object[] args);
	protected static native CLObject native_read(String s);
	protected static native CLObject native_makeSymbol(String packagename, String name);
	protected static native CLObject native_makeKeyword(String name);
	protected static native CLObject native_symbolValue(CLObject s);
	protected static native CLObject native_NIL();
	protected static native CLObject native_T();
	public static final CLObject NIL = native_NIL();
	public static final CLObject T = native_T();
	//TODO: multiple value returns
	public static CLObject funcall(String packagename, String symbolname, Object... args){
		if(!lisp_running)
			start();
		return native_funcall(packagename, symbolname, args);
	}
	public static CLObject funcall(String symbolname, Object... args){
		return funcall("COMMON-LISP", symbolname, args);
	}
	public static Object object_funcall(String packagename, String symbolname, Object... args){
		if(!lisp_running)
			start();
		return native_object_funcall(packagename, symbolname, args);
	}
	public static CLObject read(String s){
		if(!lisp_running)
			start();
		return native_read(s);
	}
	public static CLObject toLispString(String s){
		return funcall("JFFI", "CONVERT-FROM-JSTRING", s);
	}
	public static CLObject makeSymbol(String packagename, String name){
		if(!lisp_running)
			start();
		return native_makeSymbol(packagename.toUpperCase(), name.toUpperCase());
	}
	public static CLObject makeSymbol(String name){
		if(!lisp_running)
			start();
		return native_makeSymbol("COMMON-LISP", name.toUpperCase());
	}
	public static CLObject makeKeyword(String name){
		if(!lisp_running)
			start();
		return native_makeKeyword(name.toUpperCase());
	}
	public static CLObject symbolValue(CLObject s){
		if(!lisp_running)
			start();
		return native_symbolValue(s);
	}
	protected static native void native_start();
	protected static native void native_stop();
	public static void start(){
		if(!lisp_running){
			native_start();
			lisp_running = true;
		}
	}
	public static void stop(){
		if(lisp_running){
			lisp_running = false;
			native_stop();
		}
	}
	protected static native void native_attachCurrentThread() throws LispEnvironmentError;
	public static void attachCurrentThread() throws LispEnvironmentError{
		if(!lisp_running)
			start();
		native_attachCurrentThread();
	}
	protected static native void native_detachCurrentThread();
	public static void detachCurrentThread(){
		if(lisp_running)
			native_detachCurrentThread();
	}
	public static boolean isRunning(){
		return lisp_running;
	}
	protected static native void setenv(String var, String value) throws LispEnvironmentError;
	public static class Options{
		// Directory with additional ECL stuff, usually /usr/lib/ecl-16.1.3
		// This only needs to be set if ECL is installed in a different
		// path than the one specified with ./configure --prefix=...
		public static void setEcldir(String dir){
			if(dir.endsWith("/"))
				setenv("ECLDIR", dir);
			else
				setenv("ECLDIR", dir + "/");
		}
      
		public static boolean TRAP_SIGINT = false;
		public static boolean TRAP_SIGILL = false;
		public static boolean TRAP_SIGBUS = false;
		public static boolean TRAP_SIGPIPE = false;
		public static boolean TRAP_INTERRUPT_SIGNAL = true;
		public static boolean TRAP_SIGFPE = true;

		public static int BIND_STACK_SIZE = -1;
		public static int BIND_STACK_SAFETY_AREA = -1;
		public static int FRAME_STACK_SIZE = -1;
		public static int FRAME_STACK_SAFETY_AREA = -1;
		public static int LISP_STACK_SIZE = -1;
		public static int LISP_STACK_SAFETY_AREA = -1;
		public static int C_STACK_SIZE = -1;
		public static int C_STACK_SAFETY_AREA = -1;
	}
}
