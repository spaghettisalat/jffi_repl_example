DEPS=$(shell find ecl/src ecl/contrib -type f -not -name symbols_list2.h)
export ECL_TO_RUN=$(shell pwd)/ecl-android-host/bin/ecl
export LD_LIBRARY_PATH=$(shell pwd)/ecl-android-host/lib/
export ECLDIR=$(shell pwd)/ecl-android-host/lib/ecl-$(ECL_VERSION)/

all: ecl-android-target/bin/ecl

ecl-android-target/bin/ecl: $(DEPS)
	cd ecl && \
	rm -rf build && \
	./configure --host=$(HOST) --prefix=$(shell pwd)/ecl-android-target --with-cross-config=$(shell pwd)/$(HOST).cross_config && \
	make && \
	make install
