DEPS=$(shell find ecl/src ecl/contrib -type f -not -name symbols_list2.h)

all: ecl-android-host/bin/ecl

ecl-android-host/bin/ecl: $(DEPS)
	cd ecl && \
	rm -rf build && \
	./configure --prefix=$(shell pwd)/ecl-android-host $(CONFIGURE_OPTS) && \
	make && \
	make install
