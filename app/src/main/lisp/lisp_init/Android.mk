# lisp_init.c initializes the ASDF libraries. Here we link all of them
# together in a shared library.
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := lisp_init
LOCAL_SRC_FILES := lisp_init.c
LOCAL_STATIC_LIBRARIES := asdf_libs
LOCAL_SHARED_LIBRARIES := ecl
include $(BUILD_SHARED_LIBRARY)
