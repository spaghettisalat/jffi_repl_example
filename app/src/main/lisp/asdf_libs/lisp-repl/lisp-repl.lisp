(defpackage #:lisp-repl
  (:documentation "Helper-functions for setting up an interactive Lisp-REPL from Java")
  (:use #:common-lisp #:java-stream-interface)
  (:export create-repl-environment
           destroy-repl-environment
           read-eval-print-loop
           read-eval-print-output))
(in-package #:lisp-repl)

(defmacro with-repl-environment (reader writer &body body)
  (let ((input (gensym))
        (output (gensym))
        (io (gensym))
        (jreader (gensym))
        (jwriter (gensym)))
    `(let* ((,jreader ,reader)
            (,jwriter ,writer)
            (,input (make-instance 'java-character-input-stream
                                   :java-reader ,jreader))
            (,output (make-instance 'java-character-output-stream
                                    :java-writer ,jwriter))
            (,io (make-instance 'java-character-two-way-stream
                                :java-reader ,jreader
                                :java-writer ,jwriter))
            (*standard-input* ,input)
            (*standard-output* ,output)
            (*error-output* ,output)
            (*trace-output* ,output)
            (*debug-io* ,io)
            (*query-io* ,io))
       (in-package #:cl-user)
       ,@body)))

(defun destroy-repl-environment ()
  (progn
    
    (setq *standard-input* *terminal-io*
          *standard-output* *terminal-io*
          *error-output* *terminal-io*
          *trace-output* *terminal-io*
          *debug-io* *terminal-io*
          *query-io* *terminal-io*)))

(defun read-eval-print-loop (reader writer)
  (with-repl-environment reader writer
    (handler-bind ((serious-condition #'(lambda (c)
                                          (invoke-debugger c)))
                   (warning #'(lambda (c)
                                (format *error-output* "~&;;; Warning: ~a~%" c))))
      (si:top-level))))
