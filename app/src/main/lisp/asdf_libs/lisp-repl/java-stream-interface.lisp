(defpackage #:java-stream-interface
  (:documentation "Maps Lisp-Streams to Java-Streams")
  (:use #:common-lisp
        #+ecl #:gray)
  (:shadowing-import-from #:gray
                          open-stream-p output-stream-p input-stream-p
                          streamp close stream-element-type)
  (:export java-character-input-stream
           java-character-output-stream
           java-character-two-way-stream
           java-reader java-writer ioexception))
(in-package #:java-stream-interface)

;; Note: Since we have to override the stream-unread-char method, only
;; subclasses of Reader, which support mark() can be used here!
(jffi:def-class "java/io" "Reader"
  :methods (("read" () :returning :int)
            ("mark" ((readAheadLimit :int)))
            ("reset" ())
            ("ready" () :returning :boolean)
            ("close" ())))
(jffi:def-class "java/io" "Writer"
  :methods (("write" ((c :int)))
            (("write" write-string) ((s "java/lang/String")))
            ("flush" ())
            ("close" ())))

(defun ioexception-handler (stream exception)
  "Handler for a detected java-exception, presumably of type java.io.IOException"
  (if (jffi:instanceof (jffi:get-java-exception exception)
                       "java/io/IOException")
      (progn (setf (ioexception stream) exception)
             (error 'stream-error :stream stream))
      (signal exception)))
(defmacro with-ioexception-handler (stream &body body)
  "Wraps execution of body in a handler-case and calls ioexception-handler if a java-exception is detected"
  `(handler-case
       (progn ,@body)
     (jffi:java-exception (ex)
       (ioexception-handler ,stream ex))))

(defclass java-character-input-stream (fundamental-character-input-stream)
  ((java-reader :initarg :java-reader
                :accessor java-reader
                :documentation "The java.io.Reader associated with this input stream")
   (ioexception :initform nil
                :accessor ioexception
                :documentation "If an java.io.IOException is thrown during execution of some method, it is stored in this slot")))

;; TODO: non UTF-16 characters
(defmethod stream-read-char ((stream java-character-input-stream))
  (with-ioexception-handler stream
    (Reader.mark (java-reader stream) 1)
    (let ((jchar (Reader.read (java-reader stream))))
      (if (= jchar -1)
          :eof
          (code-char jchar)))))
(defmethod stream-read-char-no-hang ((stream java-character-input-stream))
  (with-ioexception-handler stream
    (when (Reader.ready (java-reader stream))
      (stream-read-char stream))))
(defmethod stream-listen ((stream java-character-input-stream))
  (with-ioexception-handler stream
    (Reader.ready (java-reader stream))))
(defmethod stream-unread-char ((stream java-character-input-stream)
                               (character t))
  (with-ioexception-handler stream
    (Reader.reset (java-reader stream))))
(defmethod stream-clear-input ((stream java-character-input-stream))
  (with-ioexception-handler stream
    (Reader.reset (java-reader stream))))
(defmethod close ((stream java-character-input-stream) &key abort)
  (declare (ignore abort))
  (with-ioexception-handler stream
    (Reader.close (java-reader stream))))

(defclass java-character-output-stream (fundamental-character-output-stream)
  ((java-writer :initarg :java-writer
                :accessor java-writer
                :documentation "The java.io.Writer associated with this input stream")
   (ioexception :initform nil
                :accessor ioexception
                :documentation "If an java.io.IOException is thrown during execution of some method, it is stored in this slot")))

(defmethod stream-write-char ((stream java-character-output-stream)
                              (character character))
  (with-ioexception-handler stream
    (Writer.write (java-writer stream) (char-code character))))
(defmethod line-column ((stream java-character-output-stream)) nil)
(defmethod stream-write-string ((stream java-character-output-stream)
                                (string t) &optional (start t) (end t))
  (with-ioexception-handler stream
    (jffi:with-unref-local ((jstr (jffi:convert-to-jstring
                                   (if (and (= start 0) (eql end nil))
                                       string
                                       (subseq string start end))
                                   :finalizer :local)))
      (Writer.write-string (java-writer stream)
                           jstr))))
(defmethod stream-finish-output ((stream java-character-output-stream))
  (with-ioexception-handler stream
    (Writer.flush (java-writer stream))))
(defmethod close ((stream java-character-output-stream) &key abort)
  (declare (ignore abort))
  (with-ioexception-handler stream
    (Writer.close (java-writer stream))))

(defclass java-character-two-way-stream (java-character-input-stream java-character-output-stream) ())
