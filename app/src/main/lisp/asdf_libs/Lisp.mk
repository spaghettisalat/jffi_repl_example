# This makefile only set up further environment variables and executes
# compile.lisp if necessary.
export HOST_ECL = $(ECL_HOST_TOPDIR)/bin/ecl
export ECLDIR = $(ECL_HOST_TOPDIR)/lib/ecl-$(ECL_VERSION)/

export LDFLAGS += $(shell $(ECL_TARGET_TOPDIR)/bin/ecl-config --ldflags) --sysroot=$(SYSROOT)
export CFLAGS += $(shell $(ECL_TARGET_TOPDIR)/bin/ecl-config --cflags) --sysroot=$(SYSROOT)
export ECL_INCLUDEDIR = $(ECL_TARGET_TOPDIR)/include/
export ECL_LIBDIR = $(ECL_TARGET_TOPDIR)/lib/

SUBDIRS:=$(shell find * -maxdepth 0 -type d)
ASDF_DEPENDENCIES:=$(shell find $(SUBDIRS))

all: asdf_libs.a

asdf_libs.a: $(ASDF_DEPENDENCIES)
	$(HOST_ECL) --shell compile.lisp
	mv asdf_libs--all-systems.a asdf_libs.a

clean:
	rm -f asdf_libs.a asdf_libs--all-systems.a jffi/libjffi_lisp.a $(patsubst %,%/*.o,$(SUBDIRS)) $(patsubst %,%/*.fas,$(SUBDIRS)) $(patsubst %,%/*.fas_host,$(SUBDIRS))
