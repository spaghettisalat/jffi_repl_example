# Build all the C files of JFFI and link them into a shared library
# together with the static library from the Lisp files of JFFI.
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := jffi
LOCAL_SRC_FILES := jffi_c_layer.c jffi_Lisp_start.c jffi_Java_pseudo_start.c
LOCAL_SHARED_LIBRARIES := ecl lisp_init
LOCAL_STATIC_LIBRARIES := jffi_lisp
# LOCAL_LDLIBS := -llog
include $(BUILD_SHARED_LIBRARY)
