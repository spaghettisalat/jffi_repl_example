# This module only makes the result of the compilation of the Lisp
# files of JFFI available to ndk-build.
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := jffi_lisp
LOCAL_SRC_FILES := libjffi_lisp.a
include $(PREBUILT_STATIC_LIBRARY)
