// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <string.h>
#include "jffi_Lisp.h"
#include "jffi_c_layer.h"

extern void init_lib_JFFI_LISP(cl_object);
extern void jffi_global_init();

static void jffi_pseudo_start(JNIEnv* env){
	char* error_message = NULL;
	cl_env_ptr cl_env1 = ecl_process_env();
	cl_object error = ECL_T;
	ECL_HANDLER_CASE_BEGIN(cl_env1, cl_cons(error, ECL_NIL)) {
		set_global_lisp_variables();
	} ECL_HANDLER_CASE(1, error) {
		error_message = ecl_base_string_pointer_safe(si_base_string_concatenate(2, cl_princ_to_string(error), make_base_string_copy("\0")));
	} ECL_HANDLER_CASE_END;
	if(error_message != NULL){
		cl_shutdown();
		const char* message_begin = "Error initialising JFFI global variables: ";
		char* message_all = malloc(strlen(error_message) + strlen(message_begin) + 1);
		strcpy(message_all, message_begin);
		strncat(message_all, error_message, strlen(error_message));
		throw_lisp_environment_error(env, message_all);
		free(message_all);
	}
}

static int jffi_set_ecl_boolean_option(JNIEnv* env, jclass lisp_options_class, int option, const char* field_name){
	jfieldID option_fieldID = (*env)->GetStaticFieldID(env, lisp_options_class, field_name, "Z");
	if(option_fieldID == NULL){
		throw_exception(env);
		return -1;
	}
	ecl_set_option(option, (*env)->GetStaticBooleanField(env, lisp_options_class, option_fieldID));
	throw_exception_if_occurred(env);
	return 0;
}

static int jffi_set_ecl_unsigned_int_option(JNIEnv* env, jclass lisp_options_class, int option, const char* field_name){
	jfieldID option_fieldID = (*env)->GetStaticFieldID(env, lisp_options_class, field_name, "I");
	if(option_fieldID == NULL){
		throw_exception(env);
		return -1;
	}
	jint option_value = (*env)->GetStaticIntField(env, lisp_options_class, option_fieldID);
	if(option_value > 0)
		ecl_set_option(option, (cl_index) option_value);
	throw_exception_if_occurred(env);
	return 0;
}

static int jffi_set_ecl_options(JNIEnv* env){
	jclass lisp_options_class = (*env)->FindClass(env, "jffi/Lisp$Options");
	if(lisp_options_class == NULL){
		throw_exception(env);
		return -1;
	}

	if(jffi_set_ecl_boolean_option(env, lisp_options_class, ECL_OPT_TRAP_SIGINT, "TRAP_SIGINT")) return -1;
	if(jffi_set_ecl_boolean_option(env, lisp_options_class, ECL_OPT_TRAP_SIGILL, "TRAP_SIGILL")) return -1;
	if(jffi_set_ecl_boolean_option(env, lisp_options_class, ECL_OPT_TRAP_SIGBUS, "TRAP_SIGBUS")) return -1;
	if(jffi_set_ecl_boolean_option(env, lisp_options_class, ECL_OPT_TRAP_SIGPIPE, "TRAP_SIGPIPE")) return -1;
	if(jffi_set_ecl_boolean_option(env, lisp_options_class, ECL_OPT_TRAP_INTERRUPT_SIGNAL, "TRAP_INTERRUPT_SIGNAL")) return -1;
	if(jffi_set_ecl_boolean_option(env, lisp_options_class, ECL_OPT_TRAP_SIGFPE, "TRAP_SIGFPE")) return -1;

	if(jffi_set_ecl_unsigned_int_option(env, lisp_options_class, ECL_OPT_BIND_STACK_SIZE, "BIND_STACK_SIZE")) return -1;
	if(jffi_set_ecl_unsigned_int_option(env, lisp_options_class, ECL_OPT_BIND_STACK_SAFETY_AREA, "BIND_STACK_SAFETY_AREA")) return -1;
	if(jffi_set_ecl_unsigned_int_option(env, lisp_options_class, ECL_OPT_FRAME_STACK_SIZE, "FRAME_STACK_SIZE")) return -1;
	if(jffi_set_ecl_unsigned_int_option(env, lisp_options_class, ECL_OPT_FRAME_STACK_SAFETY_AREA, "FRAME_STACK_SAFETY_AREA")) return -1;
	if(jffi_set_ecl_unsigned_int_option(env, lisp_options_class, ECL_OPT_LISP_STACK_SIZE, "LISP_STACK_SIZE")) return -1;
	if(jffi_set_ecl_unsigned_int_option(env, lisp_options_class, ECL_OPT_LISP_STACK_SAFETY_AREA, "LISP_STACK_SAFETY_AREA")) return -1;
	if(jffi_set_ecl_unsigned_int_option(env, lisp_options_class, ECL_OPT_C_STACK_SIZE, "C_STACK_SIZE")) return -1;
	if(jffi_set_ecl_unsigned_int_option(env, lisp_options_class, ECL_OPT_C_STACK_SAFETY_AREA, "C_STACK_SAFETY_AREA")) return -1;
	return 0;
}

static void jffi_real_start(JNIEnv* env){
	if(jffi_set_ecl_options(env))
		return;
	char* argv[1];
	argv[0] = "ecl";
	if(!cl_boot(1, argv)){
		throw_lisp_environment_error(env, "Can't boot up the Lisp environment");
		return;
	}
	//This can't be named cl_env, since this name is already used in the
	//ecl.h include file
	char* error_message = NULL;
	cl_env_ptr cl_env1 = ecl_process_env();
	cl_object error = ECL_T;
	ECL_HANDLER_CASE_BEGIN(cl_env1, cl_cons(error, ECL_NIL)) {
		ecl_init_module(NULL, init_lib_JFFI_LISP);
		set_global_lisp_variables();
		jffi_global_init();
		java_env = env;
		attached_to_lisp = 1;
	} ECL_HANDLER_CASE(1, error) {
		error_message = ecl_base_string_pointer_safe(si_base_string_concatenate(2, cl_princ_to_string(error), make_base_string_copy("\0")));
	} ECL_HANDLER_CASE_END;
	if(error_message != NULL){
		cl_shutdown();
		const char* message_begin = "Error initialising Lisp modules: ";
		char* message_all = malloc(strlen(error_message) + strlen(message_begin) + 1);
		strcpy(message_all, message_begin);
		strncat(message_all, error_message, strlen(error_message));
		throw_lisp_environment_error(env, message_all);
		free(message_all);
	}   
}

JNIEXPORT void JNICALL Java_jffi_Lisp_native_1start(JNIEnv *env, jclass mclass){
	if(pseudo_start){
		jffi_pseudo_start(env);
	}
	else{
		jffi_real_start(env);
	}
}
