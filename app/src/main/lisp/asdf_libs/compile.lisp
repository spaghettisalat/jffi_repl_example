;;; The main Lisp cross compile logic is contained in this file.
(require 'cmp)

;; First we save the current compiler configuration
(defvar *host-cc* c::*cc*)
(defvar *host-ld* c::*ld*)
(defvar *host-ar* c::*ar*)
(defvar *host-ranlib* c::*ranlib*)
(defvar *host-cc-flags* c::*cc-flags*)
(defvar *host-ecl-include-directory* c::*ecl-include-directory*)
(defvar *host-ecl-library-directory* c::*ecl-library-directory*)
(defvar *host-ld-shared-flags* c::*ld-shared-flags*)
(defvar *host-ld-bundle-flags* c::*ld-bundle-flags*)
(defvar *host-ld-flags* c::*ld-flags*)

;; Then we set up a cross compiler configuration.
(setq c::*cc* (si:getenv "CC"))
(setq c::*ld* (si:getenv "LD"))
(setq c::*ar* (si:getenv "AR"))
(setq c::*ranlib* (si:getenv "RANLIB"))
;; Only setting c::*user-cc-flags* is not enough, since c::*cc-flags*
;; still contains the cc-flags of the host compiler
(setq c::*cc-flags* (si:getenv "CFLAGS")
      c::*ecl-include-directory* (si:getenv "ECL_INCLUDEDIR")
      c::*ecl-library-directory* (si:getenv "ECL_LIBDIR"))
;; Same problem as with c::*user-cc-flags* with c::*ld-flags*
(setq c::*ld-shared-flags* (concatenate 'string "-shared "
                                        (si:getenv "LDFLAGS")))
(setq c::*ld-bundle-flags* (concatenate 'string "-shared "
                                        (si:getenv "LDFLAGS")))
(setq c::*ld-flags* (si:getenv "LDFLAGS"))

;; The API of :init-name has changed in ECL for the upcoming 16.2.0
;; version. This function emulates the old behaviour.
(defun init-name (lib-name &key monolithic)
  (if (or (string> (lisp-implementation-version) "16.1.3")
          (and (string= (lisp-implementation-version) "16.1.3")
               (string/= (ext:lisp-implementation-vcs-id) "UNKNOWN")))
      (concatenate 'string "init_lib_" lib-name
                   (if monolithic "__ALL_SYSTEMS" ""))
      lib-name))

;; Compile jffi lisp files. This is in fact enough to ensure that the
;; jffi macros are available for the compilation of other systems
;; later on.
(setf *default-pathname-defaults*
      (merge-pathnames (make-pathname :directory '(:relative "jffi"))
                       *default-pathname-defaults*))
(compile-file "jffi.lisp" :system-p t)
(c:build-static-library "jffi_lisp"
                        :lisp-files '("jffi.o")
                        :init-name #.(init-name "JFFI_LISP"))
(setf *default-pathname-defaults*
      (merge-pathnames (make-pathname :directory '(:relative :up))
                       *default-pathname-defaults*))

;; Setup ASDF: Don't put the output in a cache and register the
;; systems in the current directory.
(require 'asdf)
(asdf:disable-output-translations)
(push *default-pathname-defaults* asdf:*central-registry*)
(setq asdf:*central-registry*
      (append asdf:*central-registry*
             (directory (merge-pathnames
                         (make-pathname :directory '(:relative :wild))
                         *default-pathname-defaults*))))
;; Loading cross-compiled .fas files does not work, because they have
;; the wrong architecture. Hence we have to patch this function to
;; directly load the source file.
(defmethod asdf:perform ((o asdf:load-op) (c asdf:cl-source-file))
  (let ((file (asdf:component-pathname c)))
    (handler-case
        (load file)
     (error ()
       ;; The file can contain forms (e.g. ffi macros) which are only
       ;; valid in the native compiler. Try compiling with the host
       ;; compiler and loading the resulting .fas file.
       (let ((c::*cc* *host-cc*)
             (c::*ld* *host-ld*)
             (c::*ar* *host-ar*)
             (c::*ranlib* *host-ranlib*)
             (c::*cc-flags* *host-cc-flags*)
             (c::*ecl-include-directory* *host-ecl-include-directory*)
             (c::*ecl-library-directory* *host-ecl-library-directory*)
             (c::*ld-shared-flags* *host-ld-shared-flags*)
             (c::*ld-bundle-flags* *host-ld-bundle-flags*)
             (c::*ld-flags* *host-ld-flags*))
         (rename-file (pathname-change-type file "fas")
                      (pathname-change-type file "fas_target"))
         (handler-case
             (progn 
               (compile-file file)
               (load (pathname-change-type file "fas")))
           (error ()
             (rename-file (pathname-change-type file "fas_target")
                          (pathname-change-type file "fas"))
             (return-from asdf:perform)))
         (rename-file (pathname-change-type file "fas")
                      (pathname-change-type file "fas_host"))
         (rename-file (pathname-change-type file "fas_target")
                      (pathname-change-type file "fas")))))))
(defun pathname-change-type (pathname new-type)
  (translate-pathname pathname
                      (make-pathname :name :wild :type (pathname-type pathname))
                      (make-pathname :name :wild :type new-type)))
;; build a static library out of the asdf_libs system, whose
;; dependencies make up all the asdf systems of the app.
(asdf:make-build "asdf_libs" :type :lib
                 :init-name #.(init-name "ASDF_LIBS" :monolithic t)
                 :monolithic t)
