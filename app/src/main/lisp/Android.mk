MY_LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_PATH := $(MY_LOCAL_PATH)
include $(LOCAL_PATH)/ecl/Android.mk
LOCAL_PATH := $(MY_LOCAL_PATH)
include $(LOCAL_PATH)/asdf_libs/Android.mk
LOCAL_PATH := $(MY_LOCAL_PATH)
include $(LOCAL_PATH)/asdf_libs/jffi/AndroidLisp.mk
LOCAL_PATH := $(MY_LOCAL_PATH)
include $(LOCAL_PATH)/asdf_libs/jffi/AndroidC.mk
LOCAL_PATH := $(MY_LOCAL_PATH)
include $(LOCAL_PATH)/lisp_init/Android.mk
