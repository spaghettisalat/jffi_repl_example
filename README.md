This project is a simple Lisp REPL for Android based on [Embeddable
Common Lisp](https://common-lisp.net/project/ecl/) and
[JFFI](https://gitlab.com/spaghettisalat/jffi). It is mostly meant to
showcase how to set up the buildsystem for an Android app with ECL
using recent Android SDKs, that use gradle as the primary
buildsystem. It also integrates well with asdf: Lisp code is built
from asdf systems residing in `app/src/main/lisp/asdf_libs/`. The
cross compilation happens automatically, without the need to change
asdf system definitions.

Installation guide:
-------------------
1. Download the Android SDK and Android NDK (for the NDK, version r18
   is known to be broken, use r17c instead) and enter their paths in
	the `local.properties` file.
2. Download ECL (recommended version is 16.1.3) from
	(https://common-lisp.net/project/ecl/static/files/release/), unpack
	it, rename the folder to `ecl` and move it to the `app/src/main/ecl/`
	directory.
3. Build the project by executing `./gradlew build`.

